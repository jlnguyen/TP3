// Port to listen requests from
var port = 1234;

// Modules to be used
var express 	 = require('express');
var bodyParser = require('body-parser');
var sqlite3 	 = require('sqlite3').verbose();
var app 			 = express();
var db 		 		 = new sqlite3.Database('db.sqlite');
var ip 	   		 = require('ip');
var AES    		 = require("crypto-js/aes");
var SHA256 		 = require("crypto-js/sha256");
var cookie 		 = require('cookie');
var http    	 = require( "http" );
var url    		 = require( "url" );
var path			 = require("path");
var cookies    = require("cookie-parser");
var token  		 = "";
var myIp   		 = "";
var status 		 = false;

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/espace_user", function(req, res) {
	res.sendFile(path.join(__dirname, '/public/espace_user.html'));
});

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {

		if (err) {
			throw err;
		}

		res.json(data);
	});
});

app.get("/sessions", function(req, res, next) {
	db.all('SELECT * FROM sessions;', function(err, data) {

		if (err) {
			throw err;
		}

		res.json(data);
	});
});

app.post("/logout", function(req, res, next) {
	db.run("DELETE FROM sessions WHERE token = ?;",[token], function(err, data) {
		if (err) {
			throw err;
		}

		console.log("Cookie detruit");

		res.redirect('/');
	});

})

app.post("/login", function(req, res, next) {
	db.all("SELECT * FROM users WHERE ident = ? AND password = ? ;",[req.body.log_username,req.body.log_pass], function(err, data) {

		if (err) {
			throw err;
		}

		var debut = "";
		var fin = "";
		var charAlea = "WXCuMVDFGHo456jkl90isdfgh23zerta78ymwxTYUIOPQcvbn1JKLAZERSpqBN";

		for (var i = 0;i < 5;i++) {
			debut += charAlea.charAt(Math.floor(Math.random() * charAlea.length));
		}

		for (var i = 0;i < 5;i++) {
			fin += charAlea.charAt(Math.floor(Math.random() * charAlea.length));
		}
		if (data === undefined) {
			console.log("Data indefinie");
		}

		if (data.length == 0) {
			status = false;
		} else {
			status = true;

			ip.address();

			// operate on buffers in-place
			var buf = new Buffer(128);
			var offset = 64;
			ip.toBuffer('127.0.0.1', buf, offset);  // [127, 0, 0, 1] at offset 64
			myIp = ip.toString(buf, offset, 4);

			token = req.body.log_username+myIp+req.body.log_pass;

			console.log('Initialisation token : ' + token);

			token = SHA256(debut+token+fin).toString();

			console.log('ip : ' + myIp);
			console.log('Hachage : ' + token);

			db.all("SELECT * FROM SESSIONS WHERE ident = ? ;",[req.body.log_username], function(err, data) {
				if (data.length == 0) {
					db.run("INSERT INTO sessions (ident,token) values (?,?);",[req.body.log_username,token], function(err, data) {
					});
				} else {
					db.run("UPDATE SESSIONS SET token = ? WHERE ident = ?;",[token,req.body.log_username], function(err, data) {
					});
				}
			});
		}

		//res.sendFile(path.join(__dirname, '../public', 'espace_user.html'));

		// Set a new cookie with the name
		res.cookie('token', token, {
			httpOnly: true,
			maxAge: 60 * 60 * 24 * 1 // 1 day
		});

		res.redirect('/espace_user');

		/*res.json({
			status : status,
			token  : token
		});*/


	});
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
